# SwitchRest

Disclaimer
========

I am not responsible if you cause any damage to your Nintendo Switch or your vehicle, so proceed at your own risk.

What is SwitchRest?
===

SwitchRest is a Nintendo Switch head rest rail system which allows you to mount your Nintendo Switch to your cars head rest unit, making long hour roadtrips much more ejoyable for back seat passengers.

### Dimensions

- SwitchRest-Left / SwitchRest-Right:
  - Width: 42.32mm
  - Height: 113.12mm
  - Depth: 87.55mm

### What did you use to build this model?

I used Blender to make this 3D model.

### Files for 3D Printing

  - SwitchRest-Left (Left Rail)
  - SwitchRest-Right (Right Rail)
  - SwitchRest-Left-Right (Both Rails with fastest print position already setup)

### I want one but I don't have a 3D printer

You can use one of these online services to print your SwitchRest and have it mailed to you.

  - [Shapeways](https://www.shapeways.com/)
  - [i.materialise](https://i.materialise.com/)
  - [Sculpteo](https://www.sculpteo.com/en/)
  - [3D Hubs](https://www.3dhubs.com/)


### How can I contact you?

Your best bet is twitter: [@benmarte](https://twitter.com/benmarte)